import java.util.Random;
public class Board{
	private Tiles[][] grid;
	final int SIZE = 5;
	
	public Board(){
		Random rng = new Random();
		this.grid = new Tiles[SIZE][SIZE];
		
		for(int i = 0; i<grid.length; i++){
			int rand = rng.nextInt(grid[i].length);
			
			for(int j = 0; j<grid[i].length; j++){
				grid[i][j] = new Tiles(Tile.BLANK);
				grid[i][rand] = new Tiles(Tile.HIDDEN_WALL);
			}
		}
	}
	
	public String toString(){
		String board = "\n";
		for(Tiles[] s : grid){
			
			for(Tiles ti : s){
				board+=ti+" ";
			}
			
			board+="\n";
		}
		return board;
	}
	
	public int placeToken(int row, int col){
		if(row >= 5 || col>=5 || row<0 || col<0){
			return -2;
		}else if(grid[row][col].tile ==  Tile.CASTLE || grid[row][col].tile ==  Tile.WALL){
			return -1;
		}else if(grid[row][col].tile ==  Tile.HIDDEN_WALL){
			grid[row][col] = new Tiles(Tile.WALL);
			return 1;
		}else{
			grid[row][col] = new Tiles(Tile.CASTLE);
			return 0;
		}
	}
}
	