import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Board gameBoard = new Board();
				
		
		System.out.println("Welcome to the Castle Wall Game!");
		int numCastles = 5;
		int turns = 0;
	
		
		while(numCastles > 0 && turns < 8){
			System.out.println("Number of Castles left: "+numCastles+"\nNumber of Turns: "+turns+"\n"+gameBoard);
			
			System.out.println("Give me a row and a column less than 5 more than 0:");
			int row = reader.nextInt();
			int col = reader.nextInt();
			
			int number = gameBoard.placeToken(row, col);
			while(number<0){
				System.out.println("Reenter appropriate rows and columns");
				row = reader.nextInt();
				col = reader.nextInt();
				number = gameBoard.placeToken(row, col);
			}
			if(number==1){
				System.out.println("OOPS! There was a wall :(");
				turns++;
			}else if(number==0){
				System.out.println("Nice Job! Castle tile successfully placed!");
				turns++;
				numCastles--;
			}
		}
		System.out.println(gameBoard);
		if(numCastles==0){
			System.out.println("You won!");
		}else{
			System.out.println("You lost!");
		}
		
	}

}